<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>
    <xsl:variable name="userUse1" select="//Message[1]/From/User/@FriendlyName"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>Kanda Saikaew' s MSN Log</title>
                <style type="text/css">
                    body {font-family: Verdana, arial, sans-serif;}         
                </style>
            </head>
            <body>
               
                <a style="color: #CC0000">
                    [Conversation started on
                    <xsl:value-of select="Log/Message[1]/@Date"/>&#127;
                    <xsl:value-of select="Log/Message[1]/@Time"/>]
                </a>
                <br/>
                
                <table border="0">
                    <xsl:for-each select="Log/Message">
                        <tr>
                            <td>[
                                <xsl:value-of select="@Date"/>&#127;
                                <xsl:value-of select="@Time"/>]
                            </td>
                            <xsl:choose>
                                <xsl:when test="contains(From/User/@FriendlyName, $userUse1)">
                                    <td>
                                        <a style="color: #FFCC00"> 
                                            <xsl:value-of select="From/User/@FriendlyName"/>
                                        </a>
                                    </td>
                                    <td>:</td>
                                    <td>
                                        <a style="color: #FFCC00"> 
                                            <xsl:value-of select="Text"/>
                                        </a>
                                    </td>
                                </xsl:when>
                                <xsl:otherwise>
                                   <td>
                                        <a style="color: #66CC66"> 
                                            <xsl:value-of select="From/User/@FriendlyName"/>
                                        </a>
                                    </td>
                                    <td>:</td>
                                    <td>
                                        <a style="color: #66CC66"> 
                                            <xsl:value-of select="Text"/>
                                        </a>
                                    </td>
                                </xsl:otherwise>
                            </xsl:choose>
                        </tr>    
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>